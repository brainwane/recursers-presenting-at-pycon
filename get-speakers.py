#!/usr/bin/python3
"""Usage: python get-speakers.py

# for each program item,
# check whether any of the speakers is a Recurser
# emit a list of those speakers, printed to console and saved as file
# TODO: emit a Markdown list of those program items, titles, speakers, permalinks, dates & times

"""

# virtualenv name: RC-PyCon-speakers
# make an RC auth token and put it in ".rcauth"
# pip3 install the following

import json
import requests
import random

jsonfilename = "conference.json"
rcauthtokenfilename = ".rcauth"

with open(rcauthtokenfilename) as f:
    authtoken = f.read()


def get_speakers(pycon_json):
    """ 
    Get list of speakers out of that file
    Return a list of dictionaries: conf keys & lists of speaker names
    """

    speakers = []

    for x in pycon_json['schedule']:
        if 'authors' in x:
            speakers.append((x['conf_key'], x['authors']))

    return speakers

def find_rc_speakers(speakers):
    """
    Take a list of dictionaries
    For each; check whether any speaker is a Recurser
    GET /api/v1/people/search?q=search_string
    Return list of only talks where speaker is a Recurser
    """

    url = "https://www.recurse.com/api/v1/people/search"

    rc_speakers = []
    
    for x in speakers:
        for author in x[1]:
            payload = {'q': author, 'access_token': authtoken}
            r = requests.get(url, params=payload)
            rcresponse = r.json()
            if len(rcresponse) > 0:
                rc_speakers.append(x)

    return rc_speakers
            
def main():
    with open("conference.json") as f:
        pycon_json = json.load(f)
    pycon_speakers = get_speakers(pycon_json)
    rc_speakers = find_rc_speakers(pycon_speakers)
    outputfilename = str(random.randrange(200,499999))+".txt"
    with open(outputfilename, "w") as f:
        f.write(str(rc_speakers))
    print(rc_speakers)

if __name__=="__main__":
    main()
