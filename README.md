I go to [PyCon North America](https://us.pycon.org/) every year. Will
[Recursers](https://recurse.com/) be there, especially on stage?

This script takes a `conference.json` file (provided for download at
https://us.pycon.org/2020/schedule/conference.json - download it once
locally so you don't hit PyCon's site over and over), checks the
Recurse Center API for the speakers' names, and tells you which
Recursers are presenting (roughly), so you can look them up at the
PyCon website and in the RC directory.

TODO:

* Add Beautiful Soup support and get at the Posters page to include
  poster presenters in the search.
* Make it so the output file is Markdown that includes the
  program items, titles, speakers, permalinks, dates & times.

Disclaimers:

* It can be a bit slow; took maybe a few minutes to process about 200
  events. I oughta profile it and speed it up.
* This script is not careful, and has no tests and approximately
  no error-checking.
* Absolutely not an official project of RC or PyCon.
* [![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

Much thanks to Ernest W. Durbin III, and to the programming ecology
that helped me build this, especially the people who made Symposion
and RC's API and requests, and the many who have written excellent
documentation on Python's standard library.
